package labcodeinspection;
@SuppressWarnings("PMD.UseUtilityClass")
/**
 * @author Fabricio_Pisco
 *
 */
public class Email {  // NOPMD by fabyp on 26/5/24 19:10
	/**
	 * @author Fabricio_Pisco
	 *
	 */
	private String firstName; // could be final  // NOPMD by fabyp on 26/5/24 19:10
	/**
	 * @author Fabricio_Pisco
	 *
	 */
	private String lastName;// could be final  // NOPMD by fabyp on 26/5/24 19:10
	private String password = null; // examples of redundant initializers  // NOPMD by fabyp on 26/5/24 19:10
	/**
	 * @author Fabricio_Pisco
	 *
	 */
	private String department;
	/**
	 * @author Fabricio_Pisco
	 *
	 */
	private int defaultpasswordLength = 8; //any test  // NOPMD by fabyp on 26/5/24 19:10
	/**
	 * @author Fabricio_Pisco
	 *
	 */
	private String email;
	/**
	 * @author Fabricio_Pisco
	 *
	 */
	// reported, parameter can be declared final
	public Email(String firstName, String lastName) {  // NOPMD by fabyp on 26/5/24 19:10
		// not reported, parameter is declared final 
		this.firstName = firstName;
		// not reported, parameter is declared final
		this.lastName = lastName;
	}
	
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) { // NOPMD by fabyp on 26/5/24 19:17
		this.department = department;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) { // NOPMD by fabyp on 26/5/24 19:17
		this.email = email;
	}

	/**
	*
	*
	* @author Fabricio_Pisco
	*/
	public void showInfo() {
		System.out.println("\nFIRST NAME= " + firstName + "\nLAST NAME= " + lastName);
		System.out.println("DEPARMENT= " + department + "\nEMAIL= " + email + "\nPASSWORD= " + password);
	}
	/**
	 * @author Fabricio_Pisco
	 *
	 */
	public void setDeparment(int depChoice) {  // NOPMD by fabyp on 26/5/24 19:10
		// reported, parameter can be declared final
		switch (depChoice) {  // NOPMD by fabyp on 26/5/24 19:10
		case 1:
			this.department = "sales";
			break;
		case 2:
			this.department = "dev";
			break;
		case 3:
			this.department = "acct";
			break;
		}
	}

	private String randomPassword(int length) {  // NOPMD by fabyp on 26/5/24 19:10
		// reported, parameter can be declared final
		String set = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890#$&@*"; // if txtA will not be assigned again it is better to do this:  // NOPMD by fabyp on 26/5/24 19:10
		char[] password = new char[length];   // NOPMD by fabyp on 26/5/24 19:10
		// not reported, parameter is declared final
		for (int i = 0; i < length; i++) {
			int rand = (int) (Math.random() * set.length()); // if txtA will not be assigned again it is better to do this:  // NOPMD by fabyp on 26/5/24 19:10
			password[i] = set.charAt(rand);
		}
		return new String(password);
	}

	public void generateEmail() {  // NOPMD by fabyp on 26/5/24 19:10
		//formula to password		
		this.password = this.randomPassword(this.defaultpasswordLength);
		// violation - implicitly system-dependent conversion
		// ok - explicit system-dependent conversion
		this.email = this.firstName.toLowerCase() + this.lastName.toLowerCase() + "@" + this.department  // NOPMD by fabyp on 26/5/24 19:10
				+ ".espol.edu.ec";
	}
}
