package labcodeinspection;

import java.util.Scanner;
//NOPMD by fabyp on 26/5/24 19:10
/**
 * @author Fabricio_Pisco
 *
 */
public class EmailApp {
	private EmailApp() {
		
	}
	// NOPMD by fabyp on 26/5/24 19:10
	/**
	 * @author Fabricio_Pisco
	 * @param args
	 */
	public static void main(final String[] args) {
		Scanner sc = new Scanner(System.in);// NOPMD by fabyp on 26/5/24 19:10

		System.out.print("Enter your first name: ");// NOPMD by fabyp on 26/5/24 19:10
		String firstName = sc.nextLine();// NOPMD by fabyp on 26/5/24 19:10

		System.out.print("Enter your last name: ");// NOPMD by fabyp on 26/5/24 19:10
		String lastName = sc.nextLine();// NOPMD by fabyp on 26/5/24 19:10

		System.out.print("\nDEPARTMENT CODE\n1. for sales\n2. for Development\n3. for accounting\nEnter code: ");// NOPMD by fabyp on 26/5/24 19:10

		int depChoice = sc.nextInt();// NOPMD by fabyp on 26/5/24 19:10
		sc.close();// NOPMD by fabyp on 26/5/24 19:10

		Email email = new Email(firstName, lastName);// NOPMD by fabyp on 26/5/24 19:10
		email.setDeparment(depChoice);// NOPMD by fabyp on 26/5/24 19:10
		email.generateEmail();// NOPMD by fabyp on 26/5/24 19:10
		email.showInfo();// NOPMD by fabyp on 26/5/24 19:10
	}
}
